<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Monolog\DateTimeImmutable;


class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 150; $i++) {

            $category = $this->getReference('categoryshop-' . $faker->numberBetween(1,6));
            $product = new Product();
            $product->setTitle($faker->sentence);
            $product->setSlug($faker->slug);
            $product->setDescription($faker->text);
            $product->setOnline(true);
            $product->setSubtitle($faker->text(155));
            $product->setPrice($faker->randomFloat(2));
            $product->setImage($faker->imageUrl(640,450, 'animals', true));
            $product->setCreatedAt(new \DateTimeImmutable('now'));
            $product->setCategory($category);
            $manager->persist($product);
        }

        $manager->flush();

        $manager->flush();
    }
}
