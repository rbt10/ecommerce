<?php

namespace App\DataFixtures;

use App\Entity\CategoryShop;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryShopFixtures extends Fixture
{
    public function load(ObjectManager $manager): void{
        
        $categories = [
            [
                "name"=> "Robes",
                "slug"=> "robes"
            ],
            [
                "name"=> "T-shirt",
                "slug"=> "t-shirt"
            ],
            [
                "name"=> "Sacs",
                "slug"=> "sacs"
            ],
            [
                "name"=> "Vestes",
                "slug"=> "vestes"
            ],
            [
                "name"=> "Pantalons",
                "slug"=> "pantalons"
            ],
            [
                "name"=> "Bonnets",
                "slug"=> "bonnets"
            ],
            [
                "name"=> "Jeans",
                "slug"=> "jeans"
            ]
        ];

        foreach ($categories as $key => $value){
            $categoryshop = new CategoryShop();
            $categoryshop->setName($value['name']);
            $categoryshop->setSlug($value['slug']);
            $manager->persist($categoryshop);
            $this->addReference('categoryshop-' . $key, $categoryshop);
        }

        $manager->flush();
    }
}
