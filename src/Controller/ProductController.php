<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function  __construct( private readonly EntityManagerInterface $manager){}
    #[Route('/boutique', name: 'shop_index')]
    public function index(): Response
    {
        $products = $this->manager->getRepository(Product::class)->findAll();
        return $this->render('product/index.html.twig', [
            'product' => $products,
        ]);
    }
}
